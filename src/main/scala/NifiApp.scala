
package com

import org.apache.nifi._
import java.nio.charset._
import org.apache.nifi.spark._
import org.apache.nifi.remote.client._
import org.apache.spark._
import org.apache.nifi.events._
import org.apache.spark.streaming._
import org.apache.spark.streaming.StreamingContext._
import org.apache.nifi.remote._
import org.apache.nifi.remote.client._
import org.apache.nifi.remote.protocol._
import org.apache.spark.storage._
import org.apache.spark.streaming.receiver._
import java.io._
import org.apache.spark.serializer._

object NifiToHDFS {

  def main(args: Array[String]): Unit = {

    // Build a Site-to-site client config with NiFi web url and output port name(port name from Nifi)
    val conf = new SiteToSiteClient.Builder().url("http://localhost:8080/nifi").portName("spark").buildConfig()
    // Set an App Name
    val config = new SparkConf().setMaster("local[2]").setAppName("NifiToHDFS")
    // Create a  StreamingContext
    val ssc = new StreamingContext(config, Seconds(10))
    // Create a DStream using a NiFi receiver so that we can pull data from specified Port
    val lines = ssc.receiverStream(new NiFiReceiver(conf, StorageLevel.MEMORY_ONLY))
    // Map the data from NiFi to text, ignoring the attributes
    val text = lines.map(dataPacket => new String(dataPacket.getContent, StandardCharsets.UTF_8))
    // Save the result into hdfs file
    text.saveAsTextFiles("/user/cloudera/nifiop/")
    // Start the computation
    ssc.start()
    ssc.awaitTermination()

  }

}
